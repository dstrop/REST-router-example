<?php
declare(strict_types=1);

namespace App\RestModule\Presenters;

use App\Services;
use App\Models;
use RestRouter\Presenters\RestPresenter;

class ArticlePresenter extends RestPresenter
{

    /**
     * @inject
     * @var \App\Services\ArticleService
     */
    public $articleService;

    public function read($id)
    {
        if(!(ctype_digit($id) || is_int($id))) {
            throw new BadRequestException('Invalid parameter id.');
        }
        try {
            $article = $this->articleService->getArticle((int)$id);

            $responseData = new \stdClass;
            $responseData->title = $article->getTitle();
            $responseData->text = $article->getContent();
            $responseData->success = true;

            $this->sendJson($responseData);
        } catch (Services\ArticleNotFoundException $e) {
            throw new ResourceNotFoundException('Article not found', $e);
        }
    }

    public function update($id, $title, $content)
    {
        if(!(ctype_digit($id) || is_int($id))) {
            throw new BadRequestException('Invalid parameter id.');
        }
        if(empty($title)) {
            throw new BadRequestException('Invalid parameter title.');
        }
        if(empty($content)) {
            throw new BadRequestException('Invalid parameter content.');
        }
        try {
            $article = $this->articleService->getArticle((int)$id);

            $article->setTitle($title)
                ->setContent($content);

            $this->articleService->saveArticle($article);

            $this->sendJson(['success' => true]);
        } catch (Models\ModelValidationErrorException $e) {
            throw new BadRequestException('No changes requested.', $e);
        } catch (Services\ArticleNotFoundException $e) {
            throw new ResourceNotFoundException('Article not found', $e);
        } catch (Services\ArticlesCannotBeSaved $e) {
            throw new UnprocessableEntityException('Unable to commit changes.', $e);
        }
    }

    public function create($title, $content)
    {
        if(empty($title)) {
            throw new BadRequestException('Invalid parameter title.');
        }
        if(empty($content)) {
            throw new BadRequestException('Invalid parameter content.');
        }
        try {
            $article = new Models\ArticleModel($title, $content);

            $this->articleService->saveArticle($article);

            $this->sendJson(['success' => true]);
        } catch (Models\ModelValidationErrorException $e) {
            throw new BadRequestException('Invalid parametrs.', $e);
        } catch (Services\ArticlesCannotBeSaved $e) {
            throw new UnprocessableEntityException('Unable to save the article.', $e);
        }
    }

    public function delete($id)
    {
        if(!(ctype_digit($id) || is_int($id))) {
            throw new BadRequestException('Invalid parameter id.');
        }
        try {
            $article = $this->articleService->getArticle((int)$id);

            $this->articleService->removeArticle($article);

            $this->sendJson(['success' => true]);
        } catch (Services\ArticleNotFoundException $e) {
            throw new ResourceNotFoundException('Article not found', $e);
        } catch (Services\ArticlesCannotBeSaved $e) {
            throw new UnprocessableEntityException('Unable to commit changes.', $e);
        }
    }
}
