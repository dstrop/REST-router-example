<?php
declare(strict_types=1);

namespace App\RestModule\Presenters;

use Nette\Http;

class BadRequestException extends \Exception
{
    public function __construct(string $message = null, \Exception $previous = null)
    {
        parent::__construct($message, Http\IResponse::S400_BAD_REQUEST, $previous);
    }
}

class ResourceNotFoundException extends \Exception
{
    public function __construct(string $message = null, \Exception $previous = null)
    {
        parent::__construct($message, Http\IResponse::S400_BAD_REQUEST, $previous);
    }
}

class UnprocessableEntityException extends \Exception
{
    public function __construct(string $message = null, \Exception $previous = null)
    {
        parent::__construct($message, Http\IResponse::S500_INTERNAL_SERVER_ERROR, $previous);
    }
}