<?php
declare(strict_types=1);

namespace App\Models;

class ArticleModel
{
	
	/**
	 * @var int Article id.
	 */
	private $id;
	
	/**
	 * @var string Article title.
	 */
	private $title;
	
	/**
	 * @var string Content of the article.
	 */
	private $content;

	public function __construct(string $title, string $content)
    {
        $this->setTitle($title);
        $this->setContent($content);
    }

    /**
	 * @return int|NULL
	 */
	public function getId(): ?int
    {
		return $this->id;
	}

	public function getTitle(): string
    {
		return $this->title;
	}

	public function getContent(): string
    {
		return $this->content;
	}

	public function setId(int $id): self
    {
		$this->id = $id;
		return $this;
	}

	public function setTitle(string $title): self
    {
        if(empty($title)) {
            throw new ModelValidationErrorException('Article title cannot be empty.');
        }
		$this->title = $title;
        return $this;
	}

	public function setContent(string $content): self
    {
        if(empty($content)) {
            throw new ModelValidationErrorException('Article content cannot be empty.');
        }
		$this->content = $content;
		return $this;
	}
}
