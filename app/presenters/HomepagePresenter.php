<?php
declare(strict_types=1);

namespace App\Presenters;


class HomepagePresenter extends BasePresenter
{
    /**
     * @inject
     * @var \App\Services\ArticleService
     */
    public $articleService;

	public function renderDefault()
	{
        $this->template->articles = $this->articleService->getArticles();
	}
}
