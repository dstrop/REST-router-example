<?php
declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;
use RestRouter\Routes\RestRoute;


class RouterFactory
{
    use Nette\StaticClass;

    public static function createRouter(): RouteList
    {
        $router = new RouteList;
        $router[] = new RestRoute('api/articles', [
            RestRoute::MODULE_KEY => 'Rest',
            RestRoute::PRESENTER_KEY => 'Article'
        ]);
        $router[] = new Route('<presenter>/<action>', 'Homepage:default');
        return $router;
    }
}
