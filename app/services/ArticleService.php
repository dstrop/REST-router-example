<?php
declare(strict_types=1);

namespace App\Services;

use App\Models\ArticleModel;

class ArticleService
{
	
	/**
	 * @var ArticleModel[]
	 */
	private $articles;
	
	/**
	 * @var string Path to article storage file.
	 */
	private $articlesFile;
	
	
	public function __construct(string $articlesFile)
    {
		$this->articlesFile = $articlesFile;
		
		if(file_exists($this->articlesFile)) {
			$searializedArticles = file_get_contents($this->articlesFile);

			$this->articles = unserialize($searializedArticles);
			if(!is_array($this->articles)) {
                throw new ArticlesCannotBeLoaded;
			}
		} else {
			$this->articles = [];
			$this->saveArticles();
		}
	}
	
	/**
	 * Saves articles to articlesFile.
	 * @throws ArticlesCannotBeSaved
	 */
	private function saveArticles(): void
    {
		try {
			$searializedArticles = serialize($this->articles);
			file_put_contents($this->articlesFile, $searializedArticles);
		} catch (\Exception $e) {
			throw new ArticlesCannotBeSaved;
		}
	}
	
	/**
	 * @throws ArticlesCannotBeSaved
	 */
	public function saveArticle(ArticleModel $article): ArticleModel
    {
        if($article->getId() === NULL) {
            $article->setId($this->getNewId());
        }

		$this->articles[$article->getId()] = $article;
		
		$this->saveArticles();
		return $article;
	}
	
	/**
	 * Generates new id for article.
	 */
	private function getNewId(): int
    {
		$newId = 0;
		if(count($this->articles) > 0) {
			$lastArticle = end($this->articles);
			$newId = $lastArticle->getId() + 1;
		}
		
		return $newId;
	}
	
	
	/**
	 * @return ArticleModel[]
	 */
	public function getArticles(): array
    {
		return $this->articles;
	}
	
	/**
	 * @throws ArticleNotFoundException
	 */
	public function getArticle(int $articleId): ArticleModel
    {
		if(isset($this->articles[$articleId])) {
			return $this->articles[$articleId];
		}
		
		throw new ArticleNotFoundException;
	}
	
	/**
	 * @throws ArticlesCannotBeSaved
	 */
	public function removeArticle(ArticleModel $article): void
    {
		unset($this->articles[$article->getId()]);
		$this->saveArticles();
	}
}
