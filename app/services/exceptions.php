<?php
namespace App\Services;

class ArticleNotFoundException extends \Exception {}

class ArticlesCannotBeSaved extends \Exception {}

class ArticlesCannotBeLoaded extends \Exception {}
